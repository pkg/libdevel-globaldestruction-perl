libdevel-globaldestruction-perl (0.14-4+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 11:38:06 +0000

libdevel-globaldestruction-perl (0.14-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libdevel-globaldestruction-perl: Add :any qualifier for perl dependency.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 17:53:24 +0000

libdevel-globaldestruction-perl (0.14-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:55:57 +0100

libdevel-globaldestruction-perl (0.14-2) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:55:39 +0100

libdevel-globaldestruction-perl (0.14-1.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 03:33:44 +0000

libdevel-globaldestruction-perl (0.14-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 24 Dec 2020 14:35:58 +0100

libdevel-globaldestruction-perl (0.14-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 00:50:48 +0000

libdevel-globaldestruction-perl (0.14-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Angel Abad ]
  * Import upstream version 0.14
  * debian/copyright: Update years.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.

 -- Angel Abad <angel@debian.org>  Tue, 01 Nov 2016 11:22:44 +0100

libdevel-globaldestruction-perl (0.13-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Angel Abad ]
  * Imported Upstream version 0.13
  * debian/control: Use Build-Depends-Indep instead of Build-Depends
  * debian/copyright: Update debian/* years

 -- Angel Abad <angel@debian.org>  Tue, 26 Aug 2014 12:16:08 +0200

libdevel-globaldestruction-perl (0.12-1) unstable; urgency=low

  * Imported Upstream version 0.12
  * debian/control:
    - Build depends on debhelper (>= 9.20120312)
    - Add myself to Uploaders
  * debian/copyright: Update debian/* years
  * Bump Standards-Version to 3.9.5

 -- Angel Abad <angel@debian.org>  Sun, 03 Nov 2013 11:59:19 +0100

libdevel-globaldestruction-perl (0.11-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: drop information about removed ppport.h.
  * Relax debhelper build dependency and compatibility level.
    This package is arch:all since some time.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Wed, 10 Jul 2013 19:00:44 +0200

libdevel-globaldestruction-perl (0.09-1) unstable; urgency=low

  * New upstream release
  * Replace libsub-exporter-perl with libsub-exporter-progressive-perl in
    (build) depends

 -- Alessandro Ghedini <ghedo@debian.org>  Wed, 15 Aug 2012 18:38:01 +0200

libdevel-globaldestruction-perl (0.07-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders

 -- Alessandro Ghedini <ghedo@debian.org>  Thu, 26 Jul 2012 01:11:15 +0200

libdevel-globaldestruction-perl (0.06-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Jun 2012 15:05:00 +0200

libdevel-globaldestruction-perl (0.05-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Update d/copyright to Copyright-Format 1.0
  * Bump Standards-Version to 3.9.3
  * Bump debhelper compat level to 9

 -- Alessandro Ghedini <ghedo@debian.org>  Mon, 30 Apr 2012 12:20:21 +0200

libdevel-globaldestruction-perl (0.04-2) unstable; urgency=medium

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Dominic Hargreaves ]
  * Makefile.PL detects presence of $ {^GLOBAL_PHASE} (perl 5.14 and
    above) and becomes architecture independent, so add versioned
    {Build-,}Depends on perl (>= 5.14.0) and set package to
    Architecture: all. Thanks Jakub Wilk for the bug report and initial
    patch (closes: #648865)
  * Add a README.source with a note to backporters about the above change

 -- Dominic Hargreaves <dom@earth.li>  Tue, 15 Nov 2011 20:59:46 +0000

libdevel-globaldestruction-perl (0.04-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * No longer (build-)depend on libscope-guard-perl.
  * debian/copyright: Update for new upstream release.
  * Bump Standards-Version to 3.9.2 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 04 Jul 2011 09:50:44 +0200

libdevel-globaldestruction-perl (0.03-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Drop the XS code on perl versions recent enough to
      have ${^GLOBAL_PHASE}.
  * Use new 3.0 (quilt) source format
  * Rewrite control description
  * Drop version dependencies satisfied by oldstable
  * Standards-Version 3.9.1 (no changes)
  * Add myself to Uploaders and Copyright
  * Refresh copyright information
  * Use new short debhelper rules format
  * Bump d/compat level to 7

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 24 Dec 2010 18:17:23 -0500

libdevel-globaldestruction-perl (0.02-1) unstable; urgency=low

  * Initial Release (closes: #500222).

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Fri, 26 Sep 2008 12:12:49 +0200
